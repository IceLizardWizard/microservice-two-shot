from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVODetailEncoder(ModelEncoder):
  model = BinVO
  properties = [
    "closet_name",
    "import_href"
    ]

class ShoeListEncoder(ModelEncoder):
  model = Shoe
  properties = [
    "manufacturer",
    "model",
    "id",
    "color",
    "pic",
  ]

  def get_extra_data(self, o):
    return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
  model = Shoe
  properties = [
    "manufacturer",
    "model",
    "color",
    "pic",
    "bin",
  ]
  encoders = {
    "bin": BinVODetailEncoder(),
  }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
  if request.method == "GET":
    if bin_vo_id is not None:
      shoes = Shoe.objects.filter(bin=bin_vo_id)

    else:
      shoes = Shoe.objects.all()

    return JsonResponse(
      {"shoes": shoes},
      encoder=ShoeListEncoder,
    )

  else:
    content = json.loads(request.body)
    print("POST data", content)

    try:
      bin_href = content["bin"]
      print(bin_href)
      bin = BinVO.objects.get(import_href=bin_href)
      content["bin"] = bin

    except BinVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid Bin ID"},
        status=418,
      )

    shoe = Shoe.objects.create(**content)
    return JsonResponse(
      shoe,
      encoder=ShoeDetailEncoder,
      safe=False,
    )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe_details(request, pk):
  if request.method == "GET":
    shoe = Shoe.objects.get(id = pk)

    return JsonResponse(
      shoe,
      encoder=ShoeDetailEncoder,
      safe=False,
    )

  elif request.method == "DELETE":
    try:
      shoe = Shoe.objects.get(id=pk)
      shoe.delete()

      return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
      )
    except Shoe.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
        status=418,
      )

  else:
    content = json.loads(request.body)
    try:
      if "shoe" in content:
        shoe = Shoe.objects.get(id=pk)
        content["shoe"] = shoe

    except Shoe.DoesNotExist:
      return JsonResponse(
        {"message": "Does not exist"},
        status=418,
      )

    Shoe.objects.filter(id=pk).update(**content)
    shoe = Shoe.objects.get(id=pk)

    return JsonResponse(
      shoe,
      encoder=ShoeDetailEncoder,
      safe=False,
    )
