from django.db import models

# TODO create a VOClass for Bins
class BinVO(models.Model):
  closet_name = models.CharField(max_length=100)
  import_href = models.CharField(max_length=200, unique=True)

# Create your models here.
class Shoe(models.Model):
  manufacturer = models.CharField(max_length=100)
  model = models.CharField(max_length=100)
  color = models.CharField(max_length=100)
  pic = models.URLField(null=True)
  bin = models.ForeignKey(
    BinVO,
    related_name="shoes",
    on_delete=models.PROTECT,
  )

  def __str__(self):
    return f"{self.manufacturer} {self.model}"
