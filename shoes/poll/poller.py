import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import Shoe, BinVO

def get_bins():
    """
    This is what I get from running docker network inspect
            "Containers": {
            "26bf79f651e6c3c91d533f609c06138fbc854af848bad57a529d6fe03b2a3ea7": {
                "Name": "microservice-two-shot-wardrobe-api-1",
                might have to use that ^
                "EndpointID": "60cb96c9569050e6a550448abe8d1f0962f74be33de3bfd43c5069c107e49bee",
                "MacAddress": "02:42:ac:15:00:04",
                "IPv4Address": "172.21.0.4/16",
                "IPv6Address": ""
    """

    url = "http://wardrobe-api:8000/api/bins"
    response = requests.get(url)
    # print(response.status_code)
    content = json.loads(response.content)
    # print(content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={"closet_name": bin["closet_name"]},
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
