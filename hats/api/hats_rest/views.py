from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Hat, LocationVO
from common.json import ModelEncoder
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name",
                  "import_href",
                  "id",
                ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "color",
        "fabric",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "color",
        "fabric",
        "picture_url",
        "location",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    #for PUT check id's in hat details for updating the location for the hat

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder= HatListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        print(hat)
        return JsonResponse(
            {"hats": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "hat" in content:
                hat = Hat.objects.get(id=content["hat"])
                content["hat"] = hat
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        print(hat)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
