import React, { useEffect, useState } from 'react';

function HatsForm () {
    const [locations, setLocations] =useState([])
    const [formData, setFormData] = useState({
        style: '',
        color: '',
        fabric: '',
        picture_url: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response =await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            console.log(data);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatUrl = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                style: '',
                color: '',
                fabric: '',
                picture_url: '',
                location: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,

            [inputName]: value
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hats-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.style} placeholder="style" required type="text" name="style" id="style" className="form-control" />
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.color} placeholder="style" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="style">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="style">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={formData.picture_url} placeholder="picture_url" required type="picture_url" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="style">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default HatsForm