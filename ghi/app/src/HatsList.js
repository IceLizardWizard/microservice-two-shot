import {useState, useEffect} from 'react';

function HatsList() {
    const [hats, setHats] = useState([])


    useEffect(() => {
       async function loadHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
          const data = await response.json();
            setHats(data.hats)
        } else {
          console.error(response);
        }
      }
      loadHats();
    },[])

    const handleDelete = ((id) => {
      fetch('http://localhost:8090/api/hats/'+id,{method:"DELETE"}).then(() => {
        window.location.reload()
      })
    })


    return (
      // <table className="table table-striped">
      //   <thead>
      //     <tr>
      //       <th>Picture</th>
      //       <th>Style</th>
      //       <th>Color</th>
      //       <th>Fabric</th>
      //       <th>Location</th>
      //     </tr>
      //   </thead>
      //   <tbody>
      //     {hats.map(hat => {
      //       return (
      //         <tr key={hat.id} >
      //           <td>
      //               <img src={ hat.picture_url } />
      //             </td>
      //           <td>{ hat.style }</td>
      //           <td>{ hat.color }</td>
      //           <td>{ hat.fabric }</td>
      //           <td>{ hat.location }</td>
      //           <td><button onClick={() => handleDelete(hat.id)} className="btn btn-primary" >Delete</button></td>
      //         </tr>
      //       );
      //     })}
      //   </tbody>
      // </table>

<div className="row">
    {hats.map(hat => {
            return (
              <div key={hat.id} className="col-md-4 mb-4">
              <div  className="card shadow" style={{ width: "25rem" }}>
                <img
                src={hat.picture_url}
                className="card-img-top"
                />
                <div className="cardbody">
                  <div key={hat.id}  >
                    <div>Style: { hat.style }</div>
                    <div>Color: { hat.color} </div>
                    <div>Fabric: { hat.fabric }</div>
                    <div>Location: { hat.location }</div>
                    <div>
                      <button onClick={() => handleDelete(hat.id)} className="btn btn-primary">Delete</button>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            )
      })}
  </div>


    );
  }



  export default HatsList;