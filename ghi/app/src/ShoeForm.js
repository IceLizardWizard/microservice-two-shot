import React, {useState, useEffect} from "react";
import {useNavigate} from "react-router-dom"; // For redirecting to ListShoes after submission

function ShoeForm() {
  const [bin, setBin] = useState([]);
  const [formData, setFormData] = useState({
    manufacturer: "",
    model: "",
    color: "",
    pic: "",
  });

  const navigate = useNavigate();

  const getData = async () => {
    const url = "http://localhost:8100/api/bins";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setBin(data.bins);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const shoeUrl = "http://localhost:8080/api/shoes/";

    console.log("FormData:", formData);

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);

    console.log("Response", response);

    if (response.ok) {
      setFormData({
        manufacturer: "",
        model: "",
        color: "",
        pic: "",
        bin: "",
      });
      navigate("/shoes"); // Go back to ListShoes
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Shoes</h1>
          <form onSubmit={handleSubmit} id="add-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.manufacturer}
              placeholder="Manufacturer"
              required type="text"
              name="manufacturer"
              id="manufacturer"
              className="form-control"
              autoFocus
              />
              <label htmlFor="name">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model}
              placeholder="Model"
              required type="text"
              name="model"
              id="model"
              className="form-control"
              />
              <label htmlFor="model">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.color}
              placeholder="color"
              required type="text"
              name="color"
              id="color"
              className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.pic}
              placeholder="pic"
              // required type="url"
              // so form can be submitted if left blank
              name="pic"
              id="pic"
              className="form-control"
              />
              <label htmlFor="pic">(optional -- Image URL)</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange}
              value={formData.bin}
              required name="bin"
              id="bin"
              className="form-select"
              >
                <option value="">Choose a bin</option>
                {bin.map(bin => {
                  return (
                    <option key={bin.id} value={`/api/bins/${bin.id}/`}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
