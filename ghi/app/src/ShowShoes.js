import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

function ListShoes() {
  const [shoes, setShoes] = useState([]);

  const loadShoes = async () => {
    const response = await fetch("http://localhost:8080/api/shoes/");

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
      console.log(data);
    }
  };

  // the Stetson & Chase way
  // const handleDelete = ((id) => {
  //   fetch(`http://localhost:8080/api/shoes/${id}`,{method:"DELETE"}).then(() => {
  //     window.location.reload()
  //   })
  // })

  const handleDelete = async (id) => {
    const url = `http://localhost:8080/api/shoes/${id}/`;
    // console.log("Click");

    try {
      const response = await fetch(url, {method: "DELETE"});
      if (response.ok) {
        console.log("deleted");
        const updatedShoes = shoes.filter(shoe => shoe.id !== id);
        setShoes(updatedShoes);
      }
    } catch(e) {
      console.log("Unable to delete", e);
    }
  }

  useEffect(() =>{
    loadShoes();
  }, []);

  return (
    <div className="list-shoes-container">
      <div className="container">
        <div className="row">
          {shoes.map(shoe => {
            return (
              <div key={shoe.id} className="col-md-4 mb-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{shoe.model}</h5>
                    <p className="card-text">{shoe.manufacturer}</p>
                    <p className="card-text">{shoe.color}</p>
                    <img className="card-img-bottom" src={shoe.pic ? shoe.pic : "https://i.imgur.com/pr9uzXk.jpeg"} alt={shoe.model} />
                    <p className="card-text">{shoe.bin}</p>
                    <button className="btn btn-danger" onClick={() => handleDelete(shoe.id)}>Delete</button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <Link to="/shoes/new" className={ "btn btn-primary btn-floating position-fixed fixed-bottom fixed-right bottom-0 end-0 me-4 fade-opacity" } activeclassname="active">
        Add Shoes
      </Link>
    </div>
  );
}

export default ListShoes;
