# Wardrobify

Team:

* Chase Fienhold - Hats
* Johanson Bombaes - Shoes

## Design

## Shoes microservice

Complete the backend: create a working poller, use said poller so that views and urls work as intended. Use React to make the frontend interactive. Should be largely similar to Conference GO. Looked Link and Navigate from react-router-dom to make webapp navigation easier (Mostly for adding shoes and to redirect user back to ListShoes after submitting form). Figure out how to add default/placeholder image in case users don't submit an image. Use on hover classes to make button animations.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
Build up a backend with the informations from my models. Access said models via views. Translate views to my hats apps file which will bring nav info from a nav file I will also create.
This should be similar construction to Conference Go where I will display content in the main body and have a nav bar up top.
